package l10.tests;

import net.datastructures.LinkedBinaryTree;
import net.datastructures.Position;

public class TestCharTree {

	public static void main(String[] args) {
		// Define&Create: 
		LinkedBinaryTree<String> fBTree = new LinkedBinaryTree<String>();
		// Feed
		Position<String> fRoot = fBTree.addRoot("D");
		// First level
		Position<String>[] firstLevel = new Position[2];
		firstLevel[0] = fBTree.addLeft(fRoot, "A");
		firstLevel[1] = fBTree.addRight(fRoot, "E");
		// Second Level
//		Position<String>[] secondLevel = new Position[4];
//		secondLevel[0] = fBTree.addLeft(firstLevel[0], "C");
//		secondLevel[1] = fBTree.addRight(firstLevel[0], "D");
//		secondLevel[2] = fBTree.addLeft(firstLevel[1], "E");
//		secondLevel[3] = fBTree.addRight(firstLevel[1], "F");
//		// Third Level
//		Position<String>[] thirdLevel = new Position[2];
//		thirdLevel[0] = fBTree.addLeft(secondLevel[0], "G");
//		thirdLevel[1] = fBTree.addRight(secondLevel[0], "H");
		//
		System.out.println(TreeUtils.toStringIndentTree(fBTree));
		System.out.println("-------------------------------------------------------------");
		
		// Manipulate&Process: traversal
		System.out.println("fBTree.positions : " 	+ TreeUtils.toStringTreePositions(fBTree.positions()));
		System.out.println("fBTree.preorder : " 	+ TreeUtils.toStringTreePositions(fBTree.preorder()));
		System.out.println("fBTree.postorder : " 	+ TreeUtils.toStringTreePositions(fBTree.postorder()));
		System.out.println("fBTree.breadthfirst : " + TreeUtils.toStringTreePositions(fBTree.breadthfirst()));
		System.out.println("fBTree.inorder : " 		+ TreeUtils.toStringTreePositions(fBTree.inorder()));
		//
		System.out.println("depth of " + firstLevel[0].getElement() + ": " + fBTree.depth(firstLevel[0]));
		System.out.println("height of " + firstLevel[0].getElement() + ": " + fBTree.height(firstLevel[0]));
		System.out.println("height of tree (computed from root)" + ": " + fBTree.height(fRoot));

		System.out.println("-------------------------------------------------------------");
		Position<String> searchedPosition = searchBTreePreOrder(fBTree, "D");
		if(searchedPosition != null)
			System.out.println("Search result: " + searchedPosition.getElement());
		else
			System.out.println("Not found!");
	}
	
	public static <E> Position<E> searchBTreePreOrder(LinkedBinaryTree BTree, E searchElement){
		return searchNode(BTree, BTree.root(), searchElement);
	}
	
	private static <E> Position<E> searchNode(LinkedBinaryTree<E> BTree, Position<E> node, E searchElement){
		if (node == null)
			return null;
		
		System.out.println("___ " + node + "/" + node.getElement());
		if (node!=null && node.getElement().equals(searchElement))
			return node;
		
		Comparable<E> comparableNodeElement = (Comparable<E>)  node.getElement();
		System.out.println(comparableNodeElement);
		System.out.println(comparableNodeElement.getClass().getSimpleName());
		System.out.println(searchElement);
		System.out.println(searchElement.getClass().getSimpleName());
		System.out.println(comparableNodeElement.compareTo(searchElement));
		System.out.println();
		//Comparable<E> comparableSearchElement = (Comparable<E>)  searchElement;
		
		if (((Comparable<E>) searchElement).compareTo((E) comparableNodeElement) < 0)
			return searchNode(BTree, BTree.left(node), searchElement);
		else
			return searchNode(BTree, BTree.right(node), searchElement);
	}
}
// https://www.baeldung.com/java-string-formatter //
