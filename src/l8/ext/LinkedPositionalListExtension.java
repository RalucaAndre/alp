package l8.ext;

import java.util.Iterator;

import net.datastructures.LinkedPositionalList;
import net.datastructures.Position;

/* <Library Enhancement> 
 * 
 * */
public class LinkedPositionalListExtension<E> extends LinkedPositionalList<E> {
	// * Single-element-ops ---------------------------------------------
	// add(position_index, element) - recursivity // course
	public Position<E> add(Integer idx, E e) {
		// check size
		if (idx > this.size() - 1)
			throw new RuntimeException("Index out of range!");
		
		// get element on idx position
		Position<E> p = this.get(idx, this.first(), 0);
		// this.set(p, e);
		this.addBefore(p, e);
		return p;
	}
	// get element on idx position
	private Position<E> get(int idx, Position<E> p, int step){
		step++;
		Position<E> nextPosition = this.after(p);
		if (idx == step)
			return nextPosition;
		return get(idx, nextPosition, step);
	}
	
	// addAt(position_index, element) - with iterator // course
	public Position<E> addAt(Integer idx, E e) {
		// get element on idx position
		Iterator<Position<E>> positionIterator = this.positions().iterator();
		Integer i = -1;
		Position<E> p = null;
		while (positionIterator.hasNext() && i < idx) {
			i++;
			p = positionIterator.next();
		}
		if (i.equals(idx)) {
			//this.set(p, e); // replacing?
			this.addBefore(p, e);
			return p;
		}
		return null;
	}
	
	// removeFirst // lab
	// removeLast // lab

	// * Multiple-element-ops --------------------------------------------
	// removeFromTo(from, to) // lab

	// * Sets-Algebra-operand-ops ----------------------------------------------
	// union() // course // addAll(LinkedPositionalList) // course
	public LinkedPositionalListExtension<E> union(LinkedPositionalList<E> secondaryList){
		LinkedPositionalListExtension<E> newList = new LinkedPositionalListExtension<E>();
		Iterator<E> i = this.iterator();
		while(i.hasNext())
			newList.addLast(i.next());
		i = secondaryList.iterator();
		while(i.hasNext())
			newList.addLast(i.next());
		
		return newList;
	}
	// intersect() // lab
	// minus()

	// * Search-ops ------------------------------------------------------
	// contains(element) : Position + union.distinct
	// getAllPositionsOf(element) : Array
	// getMaxElement()
	// getMinElement()

	// * Traversal&Iterating-ops ---------------------------------------------------
	// afterPozIndex(pozIndex)
	// beforePozIndex(pozIndex)
	// indexOf(element)

	public static void main(String... args) {
		LinkedPositionalListExtension<String> pList = new LinkedPositionalListExtension<String>();
		for (int i = 0; i < 10; i++)
			pList.addLast("Element_" + i);
		System.out.println("pList:" + pList);
		// pList.addAt(4, "New_Element_4");
		pList.add(4, "New_Element_4");
		System.out.println("pList::addAt|add :" + pList);
		
		LinkedPositionalListExtension<String> sList = new LinkedPositionalListExtension<String>();
		for (int i = 10; i < 20; i++)
			sList.addLast("Element_" + i);
		System.out.println("sList: " + sList);
		
		LinkedPositionalListExtension<String> uList = pList.union(sList);
		System.out.println("uList: " + uList);
		
	}
}
