package l8.bc;

import dsaj.arrays.GameEntry;
import net.datastructures.DoublyLinkedList;

/** Class for storing high scores in an array in nondecreasing order. */
public class DoublyLinkedListScoreboard {
	// private int numEntries = 0; // number of actual entries
	// private GameDListEntry[] board; // array of game entries (names & scores)
	private DoublyLinkedList<GameEntry> board;
	private int capacity;
	public int getCapacity() {return capacity;}
	/**
	 * Constructs an empty scoreboard with the given capacity for storing entries.
	 */
	public DoublyLinkedListScoreboard(int capacity) {
		// board = new GameDListEntry[capacity];
		board = new DoublyLinkedList<GameEntry>();
		this.capacity = capacity;
	}

	/** Attempt to add a new score to the collection (if it is high enough) */
	public void add(GameEntry e) {
		int newScore = e.getScore();
		int numEntries = this.board.size();
		GameEntry nextElement;
		// board will add its first element (is empty)
		if (board.isEmpty() || board.first().getScore() < e.getScore()) {
			board.addFirst(e);
			System.out.println(">> Added-First e: " + board.first());
		}
		// board has elements (is not empty)
		else if (numEntries < this.capacity || newScore > board.last().getScore()) {
			// Prepare newBoard to get existing entries from old board plus the new element
			DoublyLinkedList<GameEntry> newBoard = new DoublyLinkedList<GameEntry>();
			boolean isAdded = false;
			// iterate over the old board by trimming the last element
			do {
				nextElement = board.removeLast();
				if (newScore < nextElement.getScore() && !isAdded) {
					newBoard.addFirst(e);
					System.out.println(">> Added e: " + newBoard.first());
					isAdded = true;
				}
				newBoard.addFirst(nextElement);
				System.out.println(">> Added from old board: " + nextElement);

			} while (!board.isEmpty());
			// replace old Board with new Board
			this.board = newBoard;

		}
		// check capacity
		if (this.board.size() > this.capacity)
			this.board.removeLast();
	}

	/** Remove and return the high score at index i. */
	public GameEntry remove(int i) throws IndexOutOfBoundsException {
		int numEntries = this.board.size();
		if (i < 0 || i >= numEntries)
			throw new IndexOutOfBoundsException("Invalid index: " + i);
		GameEntry temp = null; // save the object to be removed
		DoublyLinkedList<GameEntry> newBoard = new DoublyLinkedList<GameEntry>();
		int j = numEntries;
		while (board.last() != null) {
			j--;
			if (j == i) {
				System.out.println("Remove/skiped element: " + board.last() + "/" + j);
				temp = board.removeLast();
				continue;
			}
			newBoard.addFirst(board.removeLast());
			System.out.println(">>>> Added from old board: " + newBoard.first() + "/" + j);
		}
		this.board = newBoard;
		numEntries = this.board.size();
		return temp; // return the removed object
	}

	/** Returns a string representation of the high scores list. */
	public String toString() {
		return this.board.toString();
	}

	public static void main(String[] args) {
		// The main method
		DoublyLinkedListScoreboard highscores = new DoublyLinkedListScoreboard(5);
		String[] names = { "Rob", "Mike", "Rose", "Jill", "Jack", "Anna", "Paul", "Bob" };
		int[] scores = { 750, 1105, 590, 740, 510, 660, 720, 400 };

		for (int i = 0; i < names.length; i++) {
			GameEntry gE = new GameEntry(names[i], scores[i]);
			System.out.println("Adding " + gE + "--- into [" + highscores + "]/" + highscores.capacity);
			highscores.add(gE);
			System.out.println(" Scoreboard new: " + highscores + "--- /" + highscores.capacity);
		}

		System.out.println("Removing score at index " + 3);
		highscores.remove(3);
		System.out.println(highscores + "/" + highscores.capacity);
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);

	}
}
